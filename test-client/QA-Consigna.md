
# Ejercicio de QA - Todo List

## Introducción

El ejercicio consiste en testear y descubrir errores en una API REST. Además
de los posibles errores que podamos haber cometido, introducimos algunos a propósito.

## Entregables

- Reporte de QA con los bugs encontrados y posibles mejoras a la API
- Suite de tests automatizados a correr justificando el reporte

## Documentación de la API

La API permite crear una TodoList. se encuentra en [https://qapi-test.now.sh](https://qapi-test.now.sh) y
cuenta con los siguientes endpoints:

### Home

`GET /`

Un simple mensaje de bienvenida

### Listado de Todos

`GET /todos`

Listado de todos. No está paginado, no acepta ningún parámetro

### Single de un item

`GET /todos/:id`

Muestra un todo item en particular por id. Acepta el id numérico en la url

Ejemplo:

`GET /todos/42`

### Creación de un item

Crea un nuevo todo y lo agrega a la Lista. Acepta un objeto JSON con la información
del item

`POST /todos`

Parámetros:

- `name` (String)
- `due` (Datetime)
- `completed` (Boolean)

Ejemplo:

```
POST /todos

{
  "name": "Comer galletitas",
  "due": "2017-01-12T10:03:01Z",
  "completed": false
}
```

### Borrado de un item

Borra el item con el id especificado en la URL

`DELETE /todos/:id`

Ejemplo:

`DELETE /todos/42`

### Reset de la todo List

Borra todos los Todos. Util para testing.

`POST /todos/reset`
