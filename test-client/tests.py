import api_client
import unittest

class QapiTest(unittest.TestCase):
    def setUp(self):
        self.client = api_client.QapiClient()
        self.client.reset()

    def test_create(self):
        todo = {
            "name":"make a sandwich",
            "due": "2018-10-12T00:03:00Z",
            "completed": False
        }
        
        response1, headers = self.client.create(todo)
        todo["id"] = response1["id"]
        self.assertEqual(todo, response1)
    
    def test_delete(self):
        response, headers = self.client.create({
            "name":"make a sandwich",
            "due": "2018-10-12T00:03:00Z",
            "completed": False,
        })
        id = response["id"]
        response2, headers = self.client.delete(id)

    def test_home(self):
        response, headers = self.client.home()
        
    def test_list(self):
        todo1 = {
            "name":"make a sandwich",
            "due": "2018-10-12T00:03:00Z",
            "completed": False,
        }

        todo2 = {
            "name":"make a doughnut",
            "due": "2018-12-17T00:05:00Z",
            "completed": True,
        }

        self.client.create(todo1)
        self.client.create(todo2)
        
        response, headers = self.client.list()
        self.assertEqual(len(response), 2)
    
    def test_due(self):
        todo = {
            "name":"make an omellette",
            "due": "2018-01-12T00:03:00Z",
            "completed": False,
        }
        self.client.create(todo)

        timestamp = 1520537752 #03/08/2018
        response, headers = self.client.due(timestamp)
        self.assertEqual(len(response), 1)

if __name__ == '__main__':
    unittest.main()
