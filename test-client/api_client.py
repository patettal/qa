import urllib.request as request
import json

class QapiClient(object):

    def __init__(self):
        self.base_url = 'https://qapi-test.now.sh'

    def request(self, method='GET', path='/', data=None):
        if data:
            data = json.dumps(data)
            data = data.encode('utf-8')
        req = request.Request(self.base_url + path, data, {'Content-Type': 'application/json'}, method=method)
        f = request.urlopen(req)
        headers = f.info()
        response = f.read().decode('utf-8')
        if response:
            response = json.loads(response)
        f.close()
        return response, headers

    def home(self):
        return self.request('GET', '/')

    def list(self):
        return self.request('GET', '/todos')

    def show(self, id):
        return self.request('GET', '/todos/' + str(id))
    
    def due(self, timestamp):
        return self.request('GET', '/todos/due/' + str(timestamp))

    def create(self, data):
        return self.request('POST', '/todos', data)

    def delete(self, id):
        return self.request('DELETE', '/todos/' + str(id))

    def reset(self):
        return self.request('POST', '/todos/reset')
