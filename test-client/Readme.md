# Test Suite

La idea es que completes la suite con los tests que creas necesarios para justificar
lo dicho en el reporte

# Corriendo los tests

    $ python3 tests.py

# Accediendo a la API con el cliente:

- `self.client.home()`: `GET /`
- `self.client.list()`: `GET /todos`
- `self.client.show(42)`: `GET /todos/42`
- `self.client.create({"name": "foo", "completed": False, "due": "00..."})`: `POST /todos`
- `self.client.delete(42)`: `DELETE /todos/42`
- `self.client.reset()`: `POST /todos/reset`
