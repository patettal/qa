
# Ejercicio de QA - Todo List

## Introducción

El ejercicio consiste en testear y descubrir errores en una API REST que maneja ToDo's (o tareas). 

En el repositorio pueden encontrar una colección de POSTMAN y una pequeña suite de tests que 
escribieron los programadores de la API para cumplir con sus requerimientos de desarrollo.

Para correr los tests:
`$ python3 test-client/tests.py`

## Entregables

- Reporte de QA con los bugs encontrados y posibles mejoras a la API
- Suite de tests automatizados a correr justificando el reporte

## Documentación de la API

La API permite crear una TodoList. se encuentra en [https://qapi-test.now.sh](https://qapi-test.now.sh) y
cuenta con los siguientes endpoints:

### Home

`GET /`

Un simple mensaje de bienvenida

### Creación de un item

Crea un nuevo todo y lo agrega a la Lista. Acepta un objeto JSON con la información
del item

`POST /todos`

Parámetros:

- `name` (String)
- `due` (Datetime)
- `completed` (Boolean)

Ejemplo:

```
POST /todos

{
  "name": "Comer galletitas",
  "due": "2017-01-12T10:03:01Z",
  "completed": false
}
```

### Listado de Todos

`GET /todos`

Listado de todos. No está paginado, no acepta ningún parámetro

### Single de un item

`GET /todos/:id`

Muestra un todo item en particular por id. Acepta el id numérico en la url

Ejemplo:

`GET /todos/42`

### Listado de Todos pendientes

`GET /todos/due/:timestamp`

Muestra la lista de los todos vencidos para la fecha provista por el timestamp.

Ejemplo:

`GET /todos/due/1520537752`

### Borrado de un item

Borra el item con el id especificado en la URL

`DELETE /todos/:id`

Ejemplo:

`DELETE /todos/42`

### Reset de la todo List

Borra todos los Todos. Útil para testing.

`POST /todos/reset`
